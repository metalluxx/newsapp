//
//  OneNewsViewController.h
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "../../ApiSupport/Agregator.h"

NS_ASSUME_NONNULL_BEGIN

@interface OneNewsViewController : NSViewController{
    NSImage* cleanImage;
    NSURLSessionConfiguration *sessConfigurator;
    NSURLSession *sess;
    NSURLSessionDownloadTask *sessDownlaodTask;
    
    CALayer* calTextViewState;
    CALayer* calProgressIndicator;
    CALayer* calTitleTextField;
    CALayer* calImageView;
    CALayer* calDescriprion;
}
@property (unsafe_unretained) IBOutlet NSTextView *outletTextViewState;
@property (weak) IBOutlet NSProgressIndicator *outletProgressIndicator;
@property (weak) IBOutlet NSVisualEffectView *outletVisualEffectView;
@property (weak) IBOutlet NSTextField *outletTitleTextField;
@property (strong) IBOutlet NSView *outletView;
@property (weak) IBOutlet NSImageView *outletImageView;
@property (weak) IBOutlet NSTextField *outletDescriptionTextField;
@property (weak) IBOutlet NSButton *outletButtonBrowser;

- (IBAction)actionButtonBrowser:(id)sender;
-(void) didSelectedNewsInLVC: (id) sender;
@end


@interface OneNewsViewController (TitleSupport) <NSTextFieldDelegate>

@end

NS_ASSUME_NONNULL_END
