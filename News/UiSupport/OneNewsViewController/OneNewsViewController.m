//
//  OneNewsViewController.m
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "OneNewsViewController.h"

@interface OneNewsViewController ()

@end

@implementation OneNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // for autoupdate news before click
    [[Agregator defaultAgregator] addObserver:self forKeyPath:@"currentNews" options:0 context:nil];
    
    // image downloader initialization
    sessConfigurator = [NSURLSessionConfiguration defaultSessionConfiguration];
    sess = [NSURLSession sessionWithConfiguration:sessConfigurator];
    
    // clen image set
    cleanImage = [_outletImageView image];
    
    calTextViewState = [_outletTextViewState layer];
    calImageView = [_outletImageView layer];
    calDescriprion = [_outletDescriptionTextField layer];
    calTitleTextField = [_outletTitleTextField layer];
    calProgressIndicator = [_outletProgressIndicator layer];
//    [_outletTitleTextField addObserver:self forKeyPath:@"stringValue" options:0 context:nil];
    
}


// configure observing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == nil) {
        if([keyPath isEqualToString:@"currentNews"])
           [self didSelectedNewsInLVC:object];
        if ([keyPath isEqualToString:@"stringValue"]) {
//            CALayer *titleLayer = [_outletTitleTextField layer];
//            CATransition* titleTrans = [[CATransition alloc] init];
//            titleTrans.duration = 2.3;
//            titleTrans.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//            titleTrans.type = kCATransitionFade;
//
        }
        
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


// browser click
- (IBAction)actionButtonBrowser:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[[[Agregator defaultAgregator]currentNews]url]];
}


// fill interface information
-(void)didSelectedNewsInLVC:(id)sender{
    
    // if task dont complete
    if (sessDownlaodTask != nil)
        [sessDownlaodTask cancel];
    
    Agregator* defAgregator = [Agregator defaultAgregator];
    
    if ([defAgregator currentNews] == nil) {
        // clean UI
        [_outletDescriptionTextField setBordered:false];
        [_outletImageView setImage:nil];
        [_outletDescriptionTextField setStringValue:@""];
        [_outletDescriptionTextField setBordered:false];
        [_outletTitleTextField setStringValue:@""];
        [_outletTextViewState setString:@""];
        [_outletProgressIndicator stopAnimation:self];
        [_outletProgressIndicator setHidden:true];
        [_outletButtonBrowser setEnabled:false];
    }
    else{
        
    [_outletButtonBrowser setEnabled:true];
   
    NSString* currentTitle = [[defAgregator currentNews] title];
    [_outletTitleTextField setStringValue:currentTitle];
    [_outletDescriptionTextField setBordered:true];
        
    NSString* currentDescription = [[defAgregator currentNews] description];
    [_outletDescriptionTextField setStringValue:currentDescription];
        
    NSString* currentContent = [[defAgregator currentNews]content];
    [_outletTextViewState setString:currentContent];
    
    //Download image news
    [_outletImageView setImage:cleanImage];
    NSURL* currentUrlToImage = [[defAgregator currentNews] urlToImage];
    if(![[currentUrlToImage absoluteString]  isEqual: @""]){
        
        //start animation
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->_outletProgressIndicator setHidden:false];
            [self->_outletProgressIndicator startAnimation:self];
        });
        // create task download
        sessDownlaodTask = [sess downloadTaskWithURL:currentUrlToImage completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            //before download binary image
            NSData* binaryImage = [NSData dataWithContentsOfURL:location];
            NSImage *newsImage = [[NSImage alloc] initWithData:binaryImage];
            
            dispatch_async(dispatch_get_main_queue(), ^(){
                [self->_outletImageView setImage:newsImage];
                [self->_outletProgressIndicator stopAnimation:self];
                [self->_outletProgressIndicator setHidden:true];
            });
        }];
        [sessDownlaodTask resume];
        
    }
    else
    {
        // clean image frame
        [_outletImageView setImage:cleanImage];
        [self->_outletProgressIndicator stopAnimation:self];
        [self->_outletProgressIndicator setHidden:true];
    }
}
    

    CATransition* currentTrans = [[CATransition alloc] init];
    currentTrans.duration = 0.2;
    currentTrans.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    currentTrans.type = kCATransitionFade;
    
    calTextViewState = [_outletTextViewState layer];
    calImageView = [_outletImageView layer];
    calDescriprion = [_outletDescriptionTextField layer];
    calTitleTextField = [_outletTitleTextField layer];
    calProgressIndicator = [_outletProgressIndicator layer];
    
    [calProgressIndicator addAnimation:currentTrans forKey:@"animatePosition"];
    [calTitleTextField addAnimation:currentTrans forKey:@"animatePosition"];
    [calDescriprion addAnimation:currentTrans forKey:@"animatePosition"];
    [calImageView addAnimation:currentTrans forKey:@"animatePosition"];
    [calTextViewState addAnimation:currentTrans forKey:@"animatePosition"];
}
@end


@implementation OneNewsViewController (TitleSupport)

@end
