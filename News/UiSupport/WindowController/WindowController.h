//
//  WindowController.h
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../OneNewsViewController/OneNewsViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WindowController : NSWindowController <NSWindowDelegate>
@property (weak) IBOutlet NSWindow *outletWindow;
@end

NS_ASSUME_NONNULL_END
