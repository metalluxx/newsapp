//
//  WindowController.m
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "WindowController.h"

@interface WindowController ()
@end

@implementation WindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"kek");
    NSLog(@"%@ \n %@", segue, sender);
}

-(void)windowWillClose:(NSNotification *)notification{
    [NSApp terminate:self];
}
@end
