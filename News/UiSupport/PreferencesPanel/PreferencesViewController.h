//
//  PreferencesViewController.h
//  News
//
//  Created by Metalluxx on 18/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../../ApiSupport/Agregator.h"
#import "SourceCellView.h"
#import "../../ApiSupport/SourceItem.h"
#import "../../UiSupport/ListViewController/ListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PreferencesViewController : NSViewController

@property (weak) IBOutlet NSTableView *outletTableView;


- (IBAction)actionRemoveButton:(id)sender;
- (IBAction)actionAddButton:(id)sender;
- (IBAction)actionReloadData:(id)sender;


@end

@interface PreferencesViewController (WindowControl) <NSWindowDelegate>

@end

@interface PreferencesViewController (Table) <NSTableViewDelegate, NSTableViewDataSource>
@end

NS_ASSUME_NONNULL_END
