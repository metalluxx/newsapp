//
//  PreferencesViewController.m
//  News
//
//  Created by Metalluxx on 18/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "PreferencesViewController.h"

@interface PreferencesViewController ()

@end

@implementation PreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[[self view]window] setDelegate:self];
    // Do view setup here.
}


- (IBAction)actionRemoveButton:(id)sender {
    [[[Agregator defaultAgregator] sourceList] removeObjectAtIndex:[_outletTableView selectedRow]];
    [_outletTableView reloadData];
}

- (IBAction)actionAddButton:(id)sender {
    SourceItem* newItem = [[SourceItem alloc] init];
    [[[Agregator defaultAgregator] sourceList] addObject:newItem];
    [_outletTableView reloadData];
}

- (IBAction)actionReloadData:(id)sender {
    [[Agregator defaultAgregator] setCurrentNews:nil];
    [[[Agregator defaultAgregator] arrayNews] removeAllObjects];
    ListViewController* lvc = [[Agregator defaultAgregator] delegate];
    [lvc didLoadedNewsFromServer:self];
    [[Agregator defaultAgregator] loadNews];

}

@end

@implementation PreferencesViewController (Table)

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{
    return [[[Agregator defaultAgregator] sourceList] count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    SourceCellView* srcCell = [[SourceCellView alloc] init];
    [srcCell setCurrentSrcItem:
     [[[Agregator defaultAgregator]sourceList]objectAtIndex:row]];
    return srcCell;
}

@end

@implementation PreferencesViewController (WindowControl)

- (void)windowWillClose:(NSNotification *)notification{
    [[[Agregator defaultAgregator] userDefForAgregator] setObject:[NSKeyedArchiver archivedDataWithRootObject:[[Agregator defaultAgregator] sourceList]] forKey:@"sourceItemArray"];
}

@end
