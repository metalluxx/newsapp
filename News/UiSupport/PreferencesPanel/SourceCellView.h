//
//  SourceCellView.h
//  News
//
//  Created by Metalluxx on 18/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../../ApiSupport/SourceItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface SourceCellView : NSTableCellView
@property (weak) IBOutlet NSButton *outletCheckBox;
@property (weak) IBOutlet NSTextField *outletURL;
@property (weak) IBOutlet NSTextField *outletName;
@property SourceItem* currentSrcItem;

-(void) setEnabledUiObjects: (bool) enb;
- (IBAction)actionCheckBox:(id)sender;
- (IBAction)actionURL:(id)sender;
- (IBAction)actionName:(id)sender;


@end

NS_ASSUME_NONNULL_END
