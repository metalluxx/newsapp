//
//  SourceCellView.m
//  News
//
//  Created by Metalluxx on 18/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "SourceCellView.h"

@implementation SourceCellView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    [self addObserver:self forKeyPath:@"currentSrcItem" options:0 context:nil];
    // Drawing code here.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == nil) {
        
        if ([keyPath isEqualToString:@"currentSrcItem"]) {
            if(_currentSrcItem == nil) [self setEnabledUiObjects:false];
            else [self setEnabledUiObjects:true];
        }
        
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

-(void) setEnabledUiObjects: (bool) enb{
    [_outletURL setEnabled:enb];
    [_outletName setEnabled:enb];
    [_outletCheckBox setEnabled:enb];
}

- (IBAction)actionCheckBox:(id)sender {
}

- (IBAction)actionURL:(id)sender {
}

- (IBAction)actionName:(id)sender {
}
@end
