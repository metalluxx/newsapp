//
//  ViewController.m
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "ListViewController.h"


@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[Agregator defaultAgregator] setDelegate:self];
    [[Agregator defaultAgregator] loadNews];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{
//    NSLog(@"kek");
//    NSLog(@"%@ \n %@", segue, sender);
}

@end


@implementation ListViewController (Table)

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item{
    NSInteger res = 0;
    if (item == nil) {
        res = [[[Agregator defaultAgregator] arrayNews] count];
    }
    else{
        GroupContainsNews* currentGCN = item;
        res = [[currentGCN allNewsFromGroup] count];
    }
    return res;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item{
    id res;
    if (item == nil) {
        res = [[[Agregator defaultAgregator] arrayNews] objectAtIndex:index];
    }
    else{
        GroupContainsNews* fromItem = item;
        res = [[fromItem allNewsFromGroup] objectAtIndex:index];
    }
    return res;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item{
    if ([[item className] isEqualToString:@"OneNews"])
        return false;
    else
        return true;
}

- (NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item{
    if ([[item className] isEqualToString:@"OneNews"]) {
        
        OneNews* currentON = item;
        NewsDataCellView* dataCell = [_outletOutlineView makeViewWithIdentifier:@"DataCell" owner:self];
        [[dataCell outletDataTextField] setStringValue:currentON.title];
        return dataCell;
    }
    else{
        GroupContainsNews* currentGCN = item;
        NewsHeaderCellView* headerCell = [_outletOutlineView makeViewWithIdentifier:@"HeaderCell" owner:self];;
        [[headerCell outletHeaderTextField] setStringValue:currentGCN.groupName];
        return headerCell;
    }
    return nil;
}


- (void)outlineViewSelectionDidChange:(NSNotification *)notification{
    id currentItem = [_outletOutlineView itemAtRow:[_outletOutlineView selectedRow]];

    if ([[currentItem className] isEqualToString:@"OneNews"]) {
        [[Agregator defaultAgregator] setCurrentNews:currentItem];
    }
    else{
        [[Agregator defaultAgregator] setCurrentNews:nil];
    }
}

@end


@implementation ListViewController (AgregatorSupport) 
-(void) didLoadedNewsFromServer: (id) sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_outletOutlineView reloadData];
    });
}
@end
