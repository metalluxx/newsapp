//
//  NewsDataCellView.h
//  News
//
//  Created by Metalluxx on 19/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsDataCellView : NSTableCellView
@property (weak) IBOutlet NSTextField *outletDataTextField;
@end

NS_ASSUME_NONNULL_END
