//
//  ViewController.h
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../../ApiSupport/Agregator.h"
#import "../OneNewsViewController/OneNewsViewController.h"
#import "../../ApiSupport/GroupContainsNews.h"
#import "NewsDataCellView.h"
#import "NewsHeaderCellView.h"

@interface ListViewController : NSViewController
@property (strong) IBOutlet NSView *outletView;
@property (weak) IBOutlet NSOutlineView *outletOutlineView;
@property (weak) IBOutlet NSTableColumn *outletTableColumn;


@end

@interface ListViewController (Table) <NSOutlineViewDelegate, NSOutlineViewDataSource>
@end

@interface ListViewController (AgregatorSupport) <AgregatorDelegate>
@end

