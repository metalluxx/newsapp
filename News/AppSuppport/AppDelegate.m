//
//  AppDelegate.m
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "AppDelegate.h"
#import "Agregator.h"
#import "../UiSupport/PreferencesPanel/SourceCellView.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize def;
- (instancetype)init
{
    self = [super init];
    if (self) {
//        [[Agregator defaultAgregator] loadNews];
    }
    return self;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    NSUserDefaults* def = [[Agregator defaultAgregator] userDefForAgregator];
    NSMutableArray* sourceList = [[Agregator defaultAgregator] sourceList];
    [def setObject:[NSKeyedArchiver archivedDataWithRootObject:sourceList] forKey:@"sourceItemArray"];
}


- (IBAction)prefOpen:(id)sender {
    
    
    
}
@end
