//
//  AppDelegate.h
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
@property NSUserDefaults* def;
- (IBAction)prefOpen:(id)sender;


@end

