//
//  Agregator.h
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OneNews.h"
#import "SourceItem.h"
#import "GroupContainsNews.h"
//#import "../UiSupport/ListViewController/ListViewController.h"

NS_ASSUME_NONNULL_BEGIN



@protocol AgregatorDelegate <NSObject>
@required
-(void) didLoadedNewsFromServer: (id) sender;
@end

@interface Agregator : NSObject
{
    NSURLSessionConfiguration* sessionConfiguration;
    NSURLSession* urlSession;
    
}
@property NSUserDefaults* userDefForAgregator;
@property NSMutableArray* sourceList;
@property NSMutableArray* arrayNews;
@property OneNews* currentNews;
@property id<AgregatorDelegate> delegate;

+(Agregator*)defaultAgregator;
-(void) loadNews;
-(void) parseJSON :(NSData*)binaryJson forSourceItem: (SourceItem*) _sit;

@end



NS_ASSUME_NONNULL_END
