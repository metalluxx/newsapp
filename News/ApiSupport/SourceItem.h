//
//  SourceItem.h
//  News
//
//  Created by Metalluxx on 18/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SourceItem : NSObject
@property NSString* url;
@property NSString* name;
@property bool enabled;

-(NSURL*) getURL;
@end

@interface SourceItem (EncDec) <NSCoding>
@end

@interface SourceItem (Copy) <NSCopying>

@end
NS_ASSUME_NONNULL_END
