//
//  OneNews.m
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "OneNews.h"

@implementation OneNews
@synthesize title, description, author, urlToImage, url, publishedAt, content, source;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"";
        self.description = @"";
        self.author = @"";
        self.publishedAt = @"";
        self.content = @"";
        self.source = @"";
        self.url = [NSURL URLWithString:@""];
        self.urlToImage = [NSURL URLWithString:@""];
        self.appSource = @"";
    }
    return self;
}

- (instancetype)initWithDictonary: (NSDictionary*) dic
{
    self = [self init];
    if (self) {
        if([dic valueForKey:@"url"] == [NSNull null]) self.url = [NSURL URLWithString:@""];
        else self.url = [NSURL URLWithString:[dic valueForKey:@"url"]];
        
        if([dic valueForKey:@"urlToImage"] == [NSNull null]) self.url = [NSURL URLWithString:@""];
        else self.urlToImage = [NSURL URLWithString:[dic valueForKey:@"urlToImage"]];
        
        self.author = [dic valueForKey:@"author"];
        self.content = [dic valueForKey:@"content"];
        self.title = [dic valueForKey:@"title"];
        self.publishedAt = [dic valueForKey:@"publishedAt"];
        self.description = [dic valueForKey:@"description"];
        
        NSDictionary* binSource = [dic valueForKey:@"source"];
        self.source = [binSource valueForKey:@"name"];
    }
    return self;
}
@end
