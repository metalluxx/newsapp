//
//  SourceItem.m
//  News
//
//  Created by Metalluxx on 18/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "SourceItem.h"

@implementation SourceItem
@synthesize name, url, enabled;

-(NSURL *)getURL{
    return [NSURL URLWithString:url];
}
@end

@implementation SourceItem (EncDec)
- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    [aCoder encodeObject:name forKey:@"name"];
    [aCoder encodeObject:url forKey:@"url"];
    [aCoder encodeObject:@(enabled) forKey:@"enabled"];
}
- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    SourceItem* decodedObject = [[SourceItem alloc] init];
    decodedObject.name = [aDecoder decodeObjectForKey:@"name"];
    decodedObject.url = [aDecoder decodeObjectForKey:@"url"];
    decodedObject.enabled = [[aDecoder decodeObjectForKey:@"enabled"] boolValue];
    return decodedObject;
}
@end

@implementation SourceItem (Copy)
- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    NSData *arch = [NSKeyedArchiver archivedDataWithRootObject:self];
    SourceItem* unarch = [NSKeyedUnarchiver unarchiveObjectWithData:arch];
    return unarch;
}
@end


