//
//  OneNews.h
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OneNews : NSObject
@property NSString* title;
@property NSString* author;
@property NSString* content;
@property NSString* description;
@property NSString* publishedAt;
@property NSURL* url;
@property NSURL* urlToImage;
@property NSString* source;
@property NSString* appSource;
- (instancetype)initWithDictonary: (NSDictionary*) dic;
@end


NS_ASSUME_NONNULL_END
