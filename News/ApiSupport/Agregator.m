//
//  Agregator.m
//  News
//
//  Created by Metalluxx on 17/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "Agregator.h"

/*
[0]    (null)    @"source" : 2 key/value pairs
[1]    (null)    @"author" : @"Wes Messamore"
[2]    (null)    @"urlToImage" : @"https://www.ccn.com/wp-content/uploads/2019/02/jamie-dimon-jpmcoin-crypto-ap.jpg"
[3]    (null)    @"content" : @"JP Morgan Chase and Co. announced Thursday it would be the first major institutional bank to release its own cryptocurrency. Its new JPM Coin is an almost shockingly impotent reaction to Bitcoin and other cryptocurrencies by the United States’ largest bank.\r\n… [+6560 chars]"
[4]    (null)    @"title" : @"Someone Please Let Jamie Dimon Know That His New Cryptocurrency is a Fraud"
[5]    (null)    @"publishedAt" : @"2019-02-16T20:00:27Z"
[6]    (null)    @"description" : @"JP Morgan Chase and Co. announced Thursday it would be the first major institutional bank to release its own cryptocurrency. Its new JPM Coin is an almost shockingly impotent reaction to Bitcoin and other cryptocurrencies by the United States’ largest bank. J…"
[7]    (null)    @"url" : @"https://www.ccn.com/someone-please-let-jamie-dimon-know-that-jpm-coin-is-a-fraud"
*/



@implementation Agregator
@synthesize arrayNews;
@synthesize sourceList;
@synthesize delegate;
@synthesize currentNews;
@synthesize userDefForAgregator;

+(Agregator*)defaultAgregator{
    static Agregator* shprt;
    static dispatch_once_t shonce;
    
    dispatch_once(&shonce, ^{
        shprt = [[Agregator alloc] init];
    });
    return shprt;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        userDefForAgregator = [NSUserDefaults standardUserDefaults];
        
        if([userDefForAgregator objectForKey:@"sourceItemArray"] == nil){
            SourceItem* oneSource = [[SourceItem alloc] init];
            oneSource.name = @"Apple";
            oneSource.url = @"https://newsapi.org/v2/everything?q=apple&from=2019-02-16&to=2019-02-16&sortBy=popularity&apiKey=344b923128d14c118f7a96a5564a16cb";
            oneSource.enabled = true;
            NSMutableArray* defSourceItems = [[NSMutableArray alloc] initWithObjects:oneSource, nil];
            [userDefForAgregator setObject:[NSKeyedArchiver archivedDataWithRootObject:defSourceItems] forKey:@"sourceItemArray"];
        }
        
        sourceList = [NSKeyedUnarchiver unarchiveObjectWithData: [userDefForAgregator objectForKey:@"sourceItemArray"]];
        arrayNews = [[NSMutableArray alloc] init];
        
        sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        urlSession = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    }
    return self;
}

- (void)loadNews{
    
    for (SourceItem* sit in sourceList) {
        if([sit enabled]){
            
            
            @try {
                __block NSData* binaryNews = nil;
                
                NSURLSessionDownloadTask* downloadState = [urlSession downloadTaskWithURL:[sit getURL] completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error)
                    {
                    binaryNews = [NSData dataWithContentsOfURL:location];
                    [self parseJSON:binaryNews forSourceItem:sit];
                     }];
                
                [downloadState resume];
            } @catch (NSException *exception) {
                [delegate didLoadedNewsFromServer:self];
                NSLog(@"%@", exception);
            }
            
            
        }
    }
    

}


-(void) parseJSON :(NSData*)binaryJson forSourceItem:(nonnull SourceItem *)_sit {

    id jsonRoot = [NSJSONSerialization JSONObjectWithData:binaryJson options:NSJSONReadingAllowFragments error:nil];
    NSArray* arrayNewsDictonary = [jsonRoot valueForKey:@"articles"];
    
    GroupContainsNews* oneGroupCN = [[GroupContainsNews alloc] init];
    oneGroupCN.groupName = [_sit name];
    
    for (NSDictionary* newsDictonary in arrayNewsDictonary) {
        OneNews* news = [[OneNews alloc] initWithDictonary:newsDictonary];
        [[oneGroupCN allNewsFromGroup] addObject:news];
    }
    
    [[self arrayNews] addObject:oneGroupCN];
    [delegate didLoadedNewsFromServer:self];
}



@end
