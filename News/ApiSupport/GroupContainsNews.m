//
//  GroupContainsNews.m
//  News
//
//  Created by Metalluxx on 19/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import "GroupContainsNews.h"

@implementation GroupContainsNews
@synthesize allNewsFromGroup, groupName;

- (instancetype)init
{
    self = [super init];
    if (self) {
        groupName = @"";
        allNewsFromGroup = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
