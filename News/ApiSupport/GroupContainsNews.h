//
//  GroupContainsNews.h
//  News
//
//  Created by Metalluxx on 19/02/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SourceItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupContainsNews : NSObject
@property NSString* groupName;
@property NSMutableArray* allNewsFromGroup;

@end

NS_ASSUME_NONNULL_END
